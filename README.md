# Install Docker Compose
  1. Download the latest version of Docker Compose from the Git reposiotry:
     * sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  2. Make the downloaded binary file executable:
     * chmod +x /usr/local/bin/docker-compose
  3. Verify the Compose version:
     * docker-compose --version