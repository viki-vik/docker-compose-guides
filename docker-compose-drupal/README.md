# Deploy Drupal with Docker Compose
   1. Create a directory for drupal:
      mkdir drupal

   2. Create a drupal.yaml file inside the drupal directory:
      vi drupal/docker-compose.yaml

   3. Change the directory to drupal:
      cd drupal

   4. Start the Docker container:
      docker-compose up -d

# Run Drupal web
   1. Run docker container inspect command and Check the IP inside the drupal container:
      * docker container ps
      * docker container inspect [CONTAINER ID]

   2. Visit the URL http://your-server-ip to access the Drupal web installation wizard   

# Working with Docker Compose

##### According to guides on [HowtoForge] (https://howtoforge.com)