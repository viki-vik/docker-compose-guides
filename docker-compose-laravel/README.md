# Prerequisites
1. Ubuntu 18.04
2. Root privileges
3. docker-compose installed

# Download Laravel
  1. Download Laravel and Install Dependencies
     * git clone https://github.com/laravel/laravel.git myapp/
     * cd myapp/
  2. Install Dependencies
     * docker run --rm -v $(pwd):/app composer install
     * sudo chown -R $USER:$USER ~/myapp     
  3. Dockerizing the Laravel Project
     * create docker-compose.yml   
     * docker-compose.yml has the following configurations:
       * Laravel App Service
       * Nginx HTTP Service
       * MySQL Database Service
  4. Create Nginx Virtual Host for Laravel
     * mkdir -p nginx/{conf.d,ssl}
     * create nginx/conf.d/laravel.conf
     * copy ssl certificate file to the 'nginx/ssl/' directory:
       * sudo cp /path/to/ssl/fullchain.pem nginx/ssl/
       * sudo cp /path/to/ssl/privkey.pem nginx/ssl/

  6. Create Dockerfile for Laravel App
  7. Build the Laravel Project
     * docker-compose build
     * docker-compose up -d
     * docker-compose ps
     * docker-compose images
     * netstat -tulpn
# Laravel Post-Installation
  1. Copy the example of '.env' file and edit it inside the container:
     * cp .env.example .env
     * docker-compose exec app vim .env
  2. Change the database configuration:
       |DB_CONNECTION=mysql          |
       |DB_HOST=db                   |
       |DB_PORT=3306                 |
       |DB_DATABASE=laraveldb        |
       |DB_USERNAME=laravel          |
       |DB_PASSWORD=laravelpassworddb|
  3. Generate the Laravel application key and clear the cache configuration:
     * docker-compose exec app php artisan key:generate
     * docker-compose exec app php artisan config:cache
  4. Migrate the database:
     * docker-compose exec app php artisan migrate
  5. Open your web browser and type the domain name of your project:
     * http://laravel.your_project.io

##### According to guides on [HowtoForge] (https://howtoforge.com)

